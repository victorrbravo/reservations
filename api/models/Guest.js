// Product.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Product
let Guest = new Schema({
  GuestFirstName: {
    type: String
  },
  GuestLastName: {
    type: String
  },
  GuestDocumentNumber: {
    type: Number
  }
},{
    collection: 'Guest'
});

module.exports = mongoose.model('Guest', Guest);
