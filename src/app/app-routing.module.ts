// app-routing.module.ts

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestAddComponent } from './guest-add/guest-add.component';
import { HotelAddComponent } from './hotel-add/hotel-add.component';
import { RoomAddComponent } from './room-add/room-add.component';
import { RoomGetComponent } from './room-get/room-get.component';
import { GuestGetComponent } from './guest-get/guest-get.component';
import { HotelGetComponent } from './hotel-get/hotel-get.component';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: 'guest/create',
    component: GuestAddComponent
  },
  {
    path: 'hotel/create',
    component: HotelAddComponent
  },
  {
    path: 'room/create',
    component: RoomAddComponent
  },
  { 
    path: 'login', 
    component: LoginComponent,  
    pathMatch: 'full'
  },
  {
    path: 'hotels',
    component: HotelGetComponent
  },
  {
    path: 'rooms',
    component: RoomGetComponent
  },  
  {
    path: 'guests',
    component: GuestGetComponent
  },  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
