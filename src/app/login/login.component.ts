import { Component, OnInit } from '@angular/core';
import { GuestsService } from '../guests.service';
import { UsersService } from '../users.service';
import { Router } from '@angular/router';
import User from "../User";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  showErrorMessage = false

  constructor(private ps: GuestsService, private router: Router, private us: UsersService) { }

  ngOnInit() {
    this.us.setUserLoggedIn(null);   
    this.showErrorMessage = false  
  }

  logIn(username: string, password: string, event: Event) {
    event.preventDefault(); // Avoid default action for the submit button of the login form

    console.log("login 1")
    // Calls service to login user to the api rest
    this.ps.login(username, password).subscribe(

      res => {
        console.log("login ok");
        let u: User = {username: username};        
        this.us.setUserLoggedIn(u);
        console.log("Seteando User " + u)
        this.showErrorMessage = false 
      },
      error => {
        console.error("error " + error);
        this.showErrorMessage = true 
      },
      () => this.navigate()
    );

  }

  navigate() {
    this.router.navigateByUrl("")
  }


  

}
