// guest-add.component.ts

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GuestsService } from '../guests.service';
import Room from '../Room';
import Hotel from '../Hotel';

function changeRoom(e) {
  this.roomnumber.setValue(e.target.value, {
    onlySelf: true
  })
}

function changeGender(e) {
  this.gender.setValue(e.target.value, {
    onlySelf: true
  })
}

function changeDocumentType(e) {
  this.documenttype.setValue(e.target.value, {
    onlySelf: true
  })
}

@Component({
  selector: 'app-guest-add',
  templateUrl: './guest-add.component.html',
  styleUrls: ['./guest-add.component.css']
})
export class GuestAddComponent implements OnInit {

  showActionSuccess =  undefined;
  registrationForm: FormGroup;
  constructor(private fb: FormBuilder, private ps: GuestsService) {
    this.createForm();
    this.showActionSuccess = undefined;
  }

  City: any = ['Florida', 'South Dakota', 'Tennessee', 'Michigan']

  Genders: any = ['Masculino', 'Femenino']

  DocumentTypes: any = ['Licencia', 'Pasaporte', 'Cedula']

  isSubmitted = false;

  rooms: Room[];
  hotels: Hotel[];

  createForm() {
    //this.angForm = this.fb.group({
     // GuestName: ['', Validators.required ],
     // LastName: ['', Validators.required ],
    //  DocumentNumber: ['', Validators.required ]
   // });
   this.registrationForm = this.fb.group({
    roomnumber: [''],
    firstname: '',
    lastname: '',
    birthdate: '',
    gender: [''],
    documenttype: [''],
    documentnumber: '',
    email: '',
    phone: '',    
   })
  }

  addGuest(firstname, lastname, birthdate, gender, documenttype, documentnumber, email, phone, roomId) {
    this.ps.addGuest(firstname, lastname, birthdate, gender, documenttype, 
      documentnumber, email, 
      phone, roomId, this);    
  }

  get roomnumber() {
    return this.registrationForm.get('roomnumber');
  }
  
  get gender() {
    return this.registrationForm.get('gender');
  }

  get documentype() {
    return this.registrationForm.get('documenttype');
  }

  onSubmit() {
    this.isSubmitted = true;
    if (!this.registrationForm.valid) {
      return false;
    } else {     
      console.log("guest added OK! 1")
      this.ps.addGuest(
        this.registrationForm.get('firstname').value,
        this.registrationForm.get('lastname').value,
      '1996-06-21T16:00:00.000Z',
      this.registrationForm.get('gender').value,
      this.registrationForm.get('documenttype').value,
      this.registrationForm.get('documentnumber').value,
      this.registrationForm.get('email').value,
      this.registrationForm.get('phone').value,
      this.registrationForm.get('roomnumber').value,
      this)
    }
    

  }

  ngOnInit() {          
      this.ps
      .getRooms()
      .subscribe((data: Room[]) => {        
        this.rooms = data;                           
      });    
  }
}

