// hotel-add.component.ts

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GuestsService } from '../guests.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotel-add',
  templateUrl: './hotel-add.component.html',
  styleUrls: ['./hotel-add.component.css']
})
export class HotelAddComponent implements OnInit {

  showActionSuccess =  undefined;
  angForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private ps: GuestsService) {
    this.createForm();
    this.showActionSuccess = undefined;
  }

  createForm() {
    this.angForm = this.fb.group({
      GuestName: ['', Validators.required ],
      LastName: ['', Validators.required ],
      DocumentNumber: ['', Validators.required ]
    });
  }

  addHotel(name, address, Type) {
    console.log("Hotel addHotel")
    this.ps.addHotel(name, address, Type, this)    
    
    
  }

  ngOnInit() {
  }

}

