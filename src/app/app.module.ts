import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ProductsService } from './products.service';
import { GuestAddComponent } from './guest-add/guest-add.component';
import { HotelAddComponent } from './hotel-add/hotel-add.component';
import { HotelGetComponent } from './hotel-get/hotel-get.component';
import { RoomAddComponent } from './room-add/room-add.component';
import { RoomGetComponent } from './room-get/room-get.component';
import { GuestGetComponent } from './guest-get/guest-get.component';
import { LoginComponent } from './login/login.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';

           

@NgModule({
  declarations: [
    AppComponent,
    GuestAddComponent,
    HotelAddComponent,
    HotelGetComponent,
    RoomAddComponent,
    RoomGetComponent,
    GuestGetComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    NoopAnimationsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule 
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
