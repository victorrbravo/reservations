import { Component, OnInit } from '@angular/core';
import Room from '../Room';
import { GuestsService } from '../guests.service';

@Component({
  selector: 'app-room-get',
  templateUrl: './room-get.component.html',
  styleUrls: ['./room-get.component.css']
})

export class RoomGetComponent implements OnInit {

  rooms: Room[];
  constructor(private ps: GuestsService) { }

  ngOnInit() {
    console.log("rooms 1")
    this.ps
      .getRooms()
      .subscribe((data: Room[]) => {
        console.log("rooms *2")
        this.rooms = data;
        var i = 0;
        for (i=0; i < this.rooms.length; i++) {
          console.log(data[i].id);
          console.log(JSON.stringify(data[i]));
          console.log(this.rooms[i].Hotel);
          console.log(this.rooms[i].Hotel.name);
          console.log("");
        }
    });
    }

    deleteRoom(id, index) {      
      this.ps.deleteRoom(id).subscribe(res => {
        console.log("deleting splice room " + id)
        this.rooms.splice(index, 1);
        });
    }
}
