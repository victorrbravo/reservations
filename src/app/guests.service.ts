// guests.service.ts

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GuestsService {

  uri = 'http://localhost:3000/hotel';
  uri_room = 'http://localhost:3000/room';
  uri_guest = 'http://localhost:3000/guest';
  uri_login = 'http://localhost:3000/login';

  constructor(private http: HttpClient) { }

  addHotel(name, address, type, addcomponent) {
    const obj = {
      name,
      address,
      type
      };
      console.log("*add guest ok");
    console.log(obj);

    let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
    
    var reqHeader = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + code,
   });
  
    this.http.post(`${this.uri}`, obj,  { headers: reqHeader })
        .subscribe(res => {
          console.log('Done');
          addcomponent.showActionSuccess = true;

        })
        ;
	}

  
  addRoom(floor, number, hotelId, addcomponent) {
    const obj = {
      floor,
      number,
      hotelId
      };
      console.log("*add room ok");
    console.log(obj);

    let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
    
    var reqHeader = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + code,
   });
  
   console.log("*add room post "+ this.uri_room);
    this.http.post(`${this.uri_room}`, obj,  { headers: reqHeader })
        .subscribe(res => {           
          console.log('Done');
          addcomponent.showActionSuccess = true;
        });
	}


  addGuest(firstname, lastname, birthdate, gender, documenttype, 
    documentnumber, email, phone, roomId, addcomponent) {
    const obj = {
      firstname,
      lastname,
      birthdate,
      gender,
      documenttype,
      documentnumber,
      email,
      phone,
      roomId
      };
      
      console.log("***add guest ok");
      console.log("Obj:" + JSON.stringify(obj));

    let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
    
    var reqHeader = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + code,
   });
  
   console.log("*add room POST "+ this.uri_guest);
    this.http.post(`${this.uri_guest}`, obj,  { headers: reqHeader })
        .subscribe(res => {
          console.log('Done')
          addcomponent.showActionSuccess = true;
        });

	}

  

  // guests.service.ts


    getHotels() {
    
      let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
    
          var reqHeader = new HttpHeaders({ 
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + code,
      });
      console.log("getHotels...1");

      return this
        .http
        .get(`${this.uri}`, { headers: reqHeader });
  }

  getRooms() {
    
    let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
  
        var reqHeader = new HttpHeaders({ 
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + code,
    });
    console.log("getRooms...1");

    return this
      .http
      .get(`${this.uri_room}`, { headers: reqHeader });
}


login(username:string, password:string) {
  console.log("login 2");
  var reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json'
 });

  return this.http.post(`${this.uri_login}`, {
    username: username,
    password: password,     
  }, { headers: reqHeader });     
}
  
  getGuests() {
    let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
  
        var reqHeader = new HttpHeaders({ 
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + code,
        });
        console.log("getting GUESTS")
        return this
      .http
      .get(`${this.uri_guest}`, { headers: reqHeader });
	   }


    editGuest(id) {
  	  return this
            .http
            .get(`${this.uri}/edit/${id}`);
	    }


   updateGuest(GuestFirstName, GuestLastName, GuestDocumentNumber, id) {
   	 const obj = {
	      GuestFirstName,
	      GuestLastName,
	      GuestDocumentNumber
	    };
	    this
	      .http
	      .post(`${this.uri}/update/${id}`, obj)
	      .subscribe(res => console.log('Done'));
	      }

	deleteHotel(id) {
    let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
    
    var reqHeader = new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + code,
    });
    console.log("deleting hotel...:" + id);

   		 return this
              	.http
	              .delete(`${this.uri}/${id}`, { headers: reqHeader } );
    }
    

    deleteRoom(id) {
      let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
      
      var reqHeader = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + code,
      });
      console.log("deleting room...:" + id);
  
          return this
                  .http
                  .delete(`${this.uri_room}/${id}`, { headers: reqHeader } );
      }


      deleteGuest(id) {
        let code = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTcwMTg5OTEzLCJleHAiOjE1NzA3OTQ3MTN9.qzKYPjKTecOZEqiVM9ATv7J2sqRSOdK-_qxMREMnPpE'
        
        var reqHeader = new HttpHeaders({ 
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + code,
        });
        console.log("deleting room...:" + id);
    
            return this
                    .http
                    .delete(`${this.uri_guest}/${id}`, { headers: reqHeader } );
        }
}
