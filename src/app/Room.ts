
// Room.ts
import Hotel from './Hotel';

export default class Room {
  id: number;
  floor: number;
  number: number;
  type: string;
  hotelId: number;
  Hotel: Hotel;
}
