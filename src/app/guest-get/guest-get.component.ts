import { Component, OnInit } from '@angular/core';
import Guest from '../Guest';
import { GuestsService } from '../guests.service';

@Component({
  selector: 'app-guest-get',
  templateUrl: './guest-get.component.html',
  styleUrls: ['./guest-get.component.css']
})

export class GuestGetComponent implements OnInit {

  guests: Guest[];
  constructor(private ps: GuestsService) { }

  ngOnInit() {
    console.log("guests 1")
    this.ps
      .getGuests()
      .subscribe((data: Guest[]) => {        
        this.guests = data;

    });
    }

    deleteGuest(id, index) {      
      this.ps.deleteGuest(id).subscribe(res => {
        this.guests.splice(index, 1);
        });
    }
 
}
