// guest-add.component.ts

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GuestsService } from '../guests.service';
import Hotel from '../Hotel';

@Component({
  selector: 'app-room-add',
  templateUrl: './room-add.component.html',
  styleUrls: ['./room-add.component.css']
})
export class RoomAddComponent implements OnInit {

  showActionSuccess = undefined
  angForm: FormGroup;
  constructor(private fb: FormBuilder, private ps: GuestsService) {
    this.createForm();
    this.showActionSuccess = undefined;
  }
  hotels: Hotel[];

  createForm() {
    this.angForm = this.fb.group({
      GuestName: ['', Validators.required ],
      LastName: ['', Validators.required ],
      DocumentNumber: ['', Validators.required ]
    });
  }

  addRoom(floor, number, hotelid) {
    console.log("...*Room addRoom floor " + floor);
    console.log("...*Room addRoom number " + number);
    console.log("...*Room addRoom number " + hotelid);
    this.ps.addRoom(floor, number, hotelid, this);
  }

  ngOnInit() {    
    this.ps
    .getHotels()
    .subscribe((data: Hotel[]) => {
      console.log("hotels names data:" + data);
      this.hotels = data;
  });

  }

}

