import { Component, OnInit } from '@angular/core';
import Hotel from '../Hotel';
import { GuestsService } from '../guests.service';

@Component({
  selector: 'app-hotel-get',
  templateUrl: './hotel-get.component.html',
  styleUrls: ['./hotel-get.component.css']
})

export class HotelGetComponent implements OnInit {

  hotels: Hotel[];
  constructor(private ps: GuestsService) { }

  ngOnInit() {
    console.log("hotels 1")
    this.ps
      .getHotels()
      .subscribe((data: Hotel[]) => {
        console.log("hotels 2")
        this.hotels = data;
    });
    }

    deleteHotel(id, index) {      
      this.ps.deleteHotel(id).subscribe(res => {
        console.log("deleting splice hotel " + id)
        this.hotels.splice(index, 1);
        });
    }
}
