
// Room.ts
import Room from './Room';
import Hotel from './Hotel';

export default class Guest {
  id: number;
  firstname: string;
  lastname: string;
  birthdate: string;
  gender: string;
  documenttype: string;
  documentnumber: string;
  email:string;
  phone:string;
  roomId: number;
  Room: Room;
}
